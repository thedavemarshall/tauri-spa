import React, { Component, } from 'react';
import Victory from './Victory';
import NewGame from './NewGame';
import GameGuessFeed from './GameGuessFeed';

class GameShow extends Component {
  submitGuess = (event) => {
    event.preventDefault()

    let characters = []
    let guessInputs = document.getElementById("guess-inputs").children
    // read the inputs, the clear them out
    for (var i = 0; i < guessInputs.length; i++) {
      characters[i] = guessInputs[i].value;
      guessInputs[i].value = null;
    }

    const guess = {
      game_guess: {
        word: characters.join('')
      }
    }

    fetch(`${process.env.REACT_APP_TAURI_API_URL}/games/${this.props.gameData.slug}/guesses`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(guess)
    })
    .catch(error => {
      console.log(error)
    })
  }

  displayGuessInputs() {
    const guessInputs = []
    for (let i=0; i < this.props.gameData.solutionLength; i++) {
      guessInputs.push(<input
        type='text'
        key={i}
        id={`guess-input-${i}`}
        size='2'
        maxlength='1'
        minlength='1'
        required
      />)
    }

    return guessInputs;
  }

  render() {
    return this.props.gameData.won ? (
      <div id='game-won'>
        <Victory slug={this.props.gameData.slug} solutionLength={this.props.gameData.solutionLength}/>
        <NewGame createNewGame={this.props.createNewGame}/>
        <GameGuessFeed guesses={this.props.gameData.guesses}/>
      </div>
    ) : (
      <div id='game-show'>
        <h1 id='game-slug'>{this.props.gameData.slug}</h1>
        <form id='guess-form' onSubmit={this.submitGuess}>
          <h3>Submit a new guess:</h3>
          <div id='guess-inputs'>
            {this.displayGuessInputs()}
          </div>
          <br/>
          <input type='submit'/>
        </form>
        <GameGuessFeed guesses={this.props.gameData.guesses}/>
      </div>
    )
  }
}

export default GameShow;
