import React, { Component } from 'react'
import GameShow from './GameShow';

class Home extends Component {
  componentWillMount() {
    this.getAndSetSlug()
  }


  // This method returns a slug that corresponds to the working Game,
  // determined by the first non-null non-empty value from this algorithm
  // First check the URL for a ?game_id=SLUG query parameter
  // Next check the localStorage for a 'slug' value
  // Else have the rails_api create a new Game and read the slug from the JSON response
  getAndSetSlug() {
    return this.getSlugFromUrl() || this.getSlugFromLocalStorage() || this.getSlugFromAPI(4)
  }

  getSlugFromUrl() {
    let params = new URLSearchParams(window.location.search)
    let slug = params.get('game_id')
    if (slug && slug != 'null' && slug != '') {
      this.setSlug(slug)
      return slug
    }
  }

  getSlugFromLocalStorage() {
    let slug = localStorage.getItem('slug')
    if (slug && slug != 'null' && slug != '') {
      this.setSlug(slug)
      return slug
    }
  }

  getSlugFromAPI = (solutionLength) => {
    fetch(`${process.env.REACT_APP_TAURI_API_URL}/games`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      mode: 'cors',
      body: JSON.stringify({
        game: {
          solution_length: solutionLength
        }
      })
    })
    .then(response => response.json())
    .then(data => {
      this.setSlug(data.slug)
    }).catch(error => {
      console.log(error)
      console.log(process.env.REACT_APP_TAURI_API_URL+'/games')
    })
  }

  // Stamp out the slug to the URL and the localStorage,
  setSlug = (slug) => {
    window.history.pushState(null, null, `?game_id=${slug}`)
    localStorage.setItem('slug', slug)
    this.createWebSocket(slug)
  }

  createWebSocket = (slug) => {
    this.props.cableApp.game = this.props.cableApp.cable.subscriptions.create(
      {
        channel: 'GamesChannel',
        slug: slug
      },
      {
        received: (updatedGame) => {
          this.props.updateAppStateGame(updatedGame)
        }
      }
    )
  }

  render() {
    return <div id='home'>
      <GameShow
        gameData={this.props.gameData}
        createNewGame={this.getSlugFromAPI}
      />
    </div>

  }
}

export default Home
