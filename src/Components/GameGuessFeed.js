import React, { Component } from 'react';
import GameGuess from './GameGuess';

class GameGuessFeed extends Component {
  displayGuesses = (guesses) => {
    return guesses.map(guess => {
      return <GameGuess
        key={guess.word}
        word={guess.word}
        oxen={guess.oxen}
        cows={guess.cows}
      />
    })
  }

  render() {
    return (
      <div id='game-guess-feed'>
        <div id='guesses'>
          { this.props.guesses ? (
            this.displayGuesses(this.props.guesses)
          ) : (
            <h3>Guess the code!</h3>
          ) }
        </div>
      </div>
    )
  }
}

export default GameGuessFeed
