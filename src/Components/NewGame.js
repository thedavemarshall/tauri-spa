import React, { Component } from 'react';

class NewGame extends Component {
  constructor() {
    super()
    this.state = {
      solutionLength: []
    }
  }

  componentDidMount() {
    var data;
    fetch(`${process.env.REACT_APP_TAURI_API_URL}/games`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      mode: 'cors'
    })
    .then(response => response.json())
    .then(result => {
      this.setState({ solutionLength: result.solution_length })
    })
  }

  submitNewGame = (event) => {
    event.preventDefault()
    var data = new FormData(document.getElementById('game-form'));
    this.props.createNewGame(data.get('solutionLength'))
  }

  displayGameInputs() {
    const gameInputs = []
    gameInputs.push(<p>Solution Length:</p>)
    this.state.solutionLength.forEach(supportedValue => {
      gameInputs.push(<input name='solutionLength' type='radio' id={`solutionLength-${supportedValue}`} value={supportedValue} required/>)
      gameInputs.push(<label for={`solutionLength-${supportedValue}`}>{supportedValue}</label>)
    })
    return gameInputs;
  }

  render() {
    return (
      <div id='new-game'>
        <form id='game-form' onSubmit={this.submitNewGame}>
          {this.displayGameInputs()}
          <br/>
          <input type='submit'/>
        </form>
      </div>
    )
  }
}

export default NewGame


