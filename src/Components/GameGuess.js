import React, { Component } from 'react';

class GameGuess extends Component {
  render() {
    return (
      <div id="game-guess">
        <h3>{this.props.word}</h3>
        <h4>{"🐂".repeat(this.props.oxen) || '0 bulls'}</h4>
        <h4>{"🐄".repeat(this.props.cows) || '0 cows'}</h4>
      </div>
    )
  }
}

export default GameGuess


