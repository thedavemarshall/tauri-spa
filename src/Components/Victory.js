import React, { Component } from 'react';

class Victory extends Component {
  constructor() {
    super()

    this.state = {
      gameResultData: {
        timeString: null,
        secondsElapsed: null,
        guessCount: null
      }
    }
  }

  componentDidMount() {
    this.getGameResultData(this.props.slug)
  }

  getGameResultData = (slug) => {
    fetch(`${process.env.REACT_APP_TAURI_API_URL}/games/${this.props.slug}/results`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      mode: 'cors'
    })
    .then(response => response.json())
    .then(result => {
      var snakedResult = {
        timeString: result.time_string,
        secondsElapsed: result.seconds_elapsed,
        guessCount: result.guess_count
      }
      this.setState({ gameResultData: snakedResult })
    })
    .catch(error => console.log(error))
  }

  render() {
    return (
      <div id='victory'>
        <h2>Victory! You win!</h2>
        <p>{"🐂".repeat(this.props.solutionLength)}</p>
        <p>Got {this.props.solutionLength} bulls in {this.state.gameResultData.guessCount} guesses!</p>
        <p>Finished in {this.state.timeString}, or {this.state.gameResultData.secondsElapsed} seconds.</p>
      </div>
    )
  }
}


// props = { slug, solution_length }
export default Victory
