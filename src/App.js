import React, { Component } from 'react';
import './App.css';
import Home from './Components/Home';

class App extends Component {
  constructor() {
    super()
    this.state = {
      currentGame: {
        slug: null,
        solutionLength: null,
        guesses: [],
        won: false
      }
    }
  }

  winningGuess = (guess) => {
    return guess.oxen === this.state.currentGame.solutionLength
  }

  getGameData = (slug) => {
    fetch(`${process.env.REACT_APP_TAURI_API_URL}/games/${this.props.slug}/guesses`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      mode: 'cors'
    })
    .then(response => response.json())
    .then(result => this.updateAppStateGame(result))
    .catch(error => console.log(error))
  }

  gameWon = (game) => {
    var won = false;
    for(var i=0; i< game.guesses.length; i++) {
      if (game.guesses[i].oxen === game.solution_length) {
        won = true;
      }

    }
    return won;
  }

  updateAppStateGame = (newGame) => {
    this.setState({
      currentGame: {
        slug: newGame.slug,
        solutionLength: newGame.solution_length,
        guesses: newGame.guesses,
        won: this.gameWon(newGame)
      }
    })
  }

  render() {
    return (
      <Home
        cableApp={this.props.cableApp}
        getGameData={this.getGameData}
        gameData={this.state.currentGame}
        updateAppStateGame={this.updateAppStateGame}
      />
    );
  }

}

export default App;
